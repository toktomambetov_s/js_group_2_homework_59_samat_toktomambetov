import React, { Component, Fragment } from 'react';
import AddMovieForm from "../../components/AddMovieForm/AddMovieForm";
import Movie from "../../components/Movie/Movie";

class MovieList extends Component {
    state = {
        movies: [],
        currentMovie: '',
    };

    changeMovieName = e => this.setState({currentMovie: e.target.value});

    addMovie = () => {
        const movies = [...this.state.movies];
        movies.push({name: this.state.currentMovie});
        this.setState({movies, currentMovie: " "});
    };

    removeMovie = index => {
        const movies = [...this.state.movies];
        movies.splice(index, 1);
        this.setState({movies});
    };

    editMovieName = (e, index) => {
        console.log(e, index);
        const movies = [...this.state.movies];
        const movie = {...movies[index]};
        movie.name = e.target.value;
        movies[index] = movie;
        this.setState({movies});
    };

    render() {
        return (
            <Fragment>
                <AddMovieForm change={this.changeMovieName} currentMovieName={this.state.currentMovie} click={this.addMovie}/>
                <h3>To watch list: </h3>
                {this.state.movies.map((movie, index) => (
                    <Movie change={(e) => this.editMovieName(e, index)} key={index} name={movie.name} remove={() => this.removeMovie(index)}/>
                ))}
            </Fragment>
        );
    }
}

export default MovieList;